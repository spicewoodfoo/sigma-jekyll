---
layout: default
header-title: 
header-link: 
header-link-text: 
permalink: /leadership
---

## "It is not Title that honors the man, but man who must honor the title."

Sigma Alliance treats leadership as we do all things; with passion and verve. In our eyes leadership is not a position of privilege, or a status symbol by which one individual may lord over another. Leadership is our responsibility to those who entrust us with the direction of a community built on collaboration, vision, and perseverance. 

The quote above depicts the spirit of our ideology, the core basis on which we induct new members, and the guiding principles we espouse to steer our own actions as leaders in this amazing group.
We believe that that our community will grow and thrive based on the leadership resources we are willing and able to invest.  On this basis, development, guidance and support of new leadership is an ever present pursuit in the Sigma Alliance.

Whatever the platform, we are always seeking the opportunity to breathe new life into our ranks. If the words above reflect the type of leadership to which you aspire, it would be our honor and pleasure to bring you on board.  Simply reach out to any of our officers letting them know that you're interested in becoming a part of Sigma Alliance Leadership and help us make Sigma a community we can all be proud of! 
