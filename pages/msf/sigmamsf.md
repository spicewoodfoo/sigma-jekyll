---
layout: page-msf-sigma
title: About
permalink: /msf/sigma
section: msf
intro_paragraph: >
  "All your (MSF) bases are belong to us"
---

## The Sigma philosophy:
Unlike other groups of alliances, The SIGMA cluster acts like a single entity.

The purpose is to create an environment to encourage any type of movement desired by the individual member� For example, a player whose focus is aggressive growth would not be a good fit for a low growth alliance. Rather than that player choosing to leave, they are encouraged to apply and specify the power or specs of an alliance and can move up.

The goal is to retain all players who want to put in the effort to make their experience the best they can. If one alliance doesn�t meet their needs, we can find them another home with a different alliance better suited for their playing style.

### SIGMA Has the Support of:
   * A Community of 780+ members
   * 24 Alliances
   * A Fully Organized and Strong Leadership
   * Constant STP/CP growth for You & Your Alliance
   * Organized and structured alliances
   * Structured player movements

### The SIGMA Community Has a Strong List of Achievements:
   * Ultimus VII 68%
   * Strike Raid Alpha IV 100%
   * Strike Raid Beta IV 100%
   * Strike Raid Gamma IV 100%
   * Multiple Alliances ranked Top 300 or Better!
   * Multiple Alliances ranked top 100 or Better!
   * Multiple alliances regularly in the top 2% in Raid Rankings
   * Multiple alliances in the top 1-3% in Alliance War Rankings
   * Multiple alliances ranked top 100 in War League Trophies
   * Multiple alliances in Gold 2 or Better league!


<h4 style="margin-top: 0; padding-bottom: 0;"><a href="https://discord.gg/q7v3JADsXH" target="_blank" class="biglogo-cta" style="margin-top: 2rem;">
 Join Our Discord </a></h4>