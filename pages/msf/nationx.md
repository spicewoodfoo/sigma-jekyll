---
layout: page-msf-nationx
title: Nation-X
permalink: /msf/nationx
section: nationx
intro_paragraph: >
  "Are you ready to enhance your MSF experience?"
---
## About

We are a group of INDEPENDENT alliances sharing a single server for mutual recruitment benefits. We have some cluster rules which includes member behaviour, coordination between NX Alliances, which are explained below.


## The NX Philosophy:

Nation X operates as a large cluster of affiliated alliances, but each alliance retains its autonomy. Alliances set their own goals and make all decisions regarding their members. Alliances are able to move members to find the best fit for each individual and the alliances. 

Recruiting and subsequent movement are integral to the operation and growth of the cluster as a whole. Retention of members is also a key goal of the cluster, and any member being moved out of an alliance should be given any and all assistance in finding a spot with another cluster alliance.

All Members of Nation-X are in this game together, whether hardcore, competitive, semi-casual, or casual, we all want to have fun. No Member and no Player deserves to be treated poorly or have a bad experience here. Even players that don't seem to fit very well in a specific Alliance still need to be treated with respect!

A player with a not-so-stellar roster, spotty attendance, or inconsistent damage, is still a person and deserves to be treated with respect. While they may not be a perfect fit for your Alliance, there are other Alliances in the Nation-X Cluster that might be a better fit. We will try our best to help find them a home in another Alliance that meets the needs of both the Member and the Alliance.

We know emotions can run high as everyone wants their time to be as productive as possible, working together to achieve difficult milestones and receive the best rewards they possibly can. Remember, we're all in this together.

Nation-X will not tolerate disrespect and poor treatment of any Member of our Family. We are all for having fun here, but only with friends that can understand this. Thanks for being here and we appreciate everyone here in the Nation-X Family.

### Nation-X Has the Support of:
- A Community of 1,100+ members
- 30+ Alliances
- A Fully Organized and Strong Leadership
- Direct access to well known Content Creators
- Constant STP/CP growth for You & Your Alliance


### The Nation-X Community Has a Strong List of Achievements:
- Ultimus VII 40%
- Ultimus VII 30%
- Ultimus VI 100%
- Greek Raids IV 100%
- Thanos III 100%
- Deadpool El Scorcho 100%


### Nation-X Alliance Rankings Include:
- 1-2%, 
- 3-10%, 
- 11-25%, 
- 26-50% Season Rewards


### Alliance Wars:
- War Leagues from Silver II to Gold II


### Collection Power Rankings:
- Total Collection Power Rankings from 90-3000**


###### We have limited openings for a few Alliances that can contribute to the overall health of our Community.

{% include msf-join-nationx.html %}

#### Nation-X - Alliance List Image:
[Click here to open Original image source for most current information](https://drive.google.com/thumbnail?id=1DF41qnOMonNNUn05gSUrM9_sxgiw5L1Y&sz=w800-h1000)
![](https://drive.google.com/thumbnail?id=1DF41qnOMonNNUn05gSUrM9_sxgiw5L1Y&sz=w800-h1000) 


<h4 style="margin-top: 0; padding-bottom: 0;"><a href="https://discord.gg/q7v3JADsXH" target="_blank" class="biglogo-cta" style="margin-top: 2rem;">
 Join Our Discord </a></h4>