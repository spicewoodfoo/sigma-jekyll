---
layout: default
header-title: Sigma Alliance Mandalorians
gg-link: https://swgoh.gg/g/53613/sigma-alliance-mandalorians/
gg-link-text: visit swgoh.gg profile
guild-photo: /assets/img/mando.jpg
short-description: Sigma Alliance Mandalorians
intro_paragraph: |
  You can edit edit `pages/guilds/mandalorians.md` in a text editor.
---

### About Us

GL: Broksby <br>
Guild Reset 10:30Pm UTC/5:30pm est

### Requirements

* 2.4 million Character GP
* 600 daily
* Both Revans
* HMF unlocked
* G11+ Separatists
* unlocked Darth Malak

#### T7 Pit

* 8PM 24hr join period
* Zerg when opens

#### HAAT

* 8pm 24 hr join period
* Zerg when opens

#### HSTR

* 8PM 24hr join period
* Zerg when opens

[Join Sigma Alliance!](https://discord.gg/V33Kfaj)
