---
layout: default
header-title: Sigma Alliance Rho
gg-link: https://swgoh.gg/g/4969/sigma-alliance-rho/
gg-link-text: visit swgoh.gg profile
guild-photo: /assets/img/Rho.jpg
short-description: Sigma Alliance Rho
intro_paragraph: You can edit edit `pages/guilds/Rho.md` in a text editor.
---

### About Us

Rho joined Sigma Alliance in May 2019. We are an European guild, with a sizable French and Swedish member base. Rho is an active community within the vast member base of Sigma Alliance. When you're a member of Rho, you're also a member of the Sigma Alliance. One big guild, built in to different tiers, based on activity, roster and performance, both US and EU based.


### Requirements

In order to join Rho, you are required to
* Have a 3 million GP
* Have Discord
* Have a swgoh.gg account
* Have hSTR ready rooster.
* Be able to complete 90 Combat waves in TB
* Contribute 600 tickets daily


### Raid Info

#### The Pit

* Sim Raid Heroic Pit is launced on auto at 17:00hrs UTC.
* We use join period of 24hrs.

#### hAAT Raid:

* Raid Launch is at 18:00hrs UTC.
* We use join period of 24hrs.

#### Hstr Raid:

* Raid Launch is at 18:00hrs UTC.
* We use join period of 24hrs.


[Join Sigma Alliance!](https://discord.gg/V33Kfaj)

Hope to see you soon!