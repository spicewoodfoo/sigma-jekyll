---
layout: default
header-title: Sigma Alliance Beta
gg-link: https://swgoh.gg/g/50755/sigma-alliance-b/
gg-link-text: visit swgoh.gg profile
guild-photo: /assets/img/beta.jpg
short-description: Sigma Alliance Beta
intro_paragraph: |
  You can edit edit `pages/guilds/beta.md` in a text editor.
---

### About Us

Beta was formed in August 2018 after multiple guilds from outside of SIGMA merged and joined The Alliance. This guild requires participation in all aspects of SWGOH, with an added emphasis on Territory Battles and the Heroic Sith Triumvirate Raid. Our guild reset is at 18:30 UTC. 

* Guild Leader: Quatern 
* Officers: malvegil, Maz897, ObiShenobi, Shonkathonk, SithHunter, stiiillωaiting, Stoka, Veloxraptor 

### Requirements

* Minimum 4.0 Million GP
* TW and GeoTB participation to the best of your ability
* Required LS/DS TB Characters at required level

### Raid Info

#### hRancor Raid:

* Launch at 9pm UTC with a 23hr join period
* Simmed for simplicity

#### hAAT Raid:

* Launch at 8pm UTC with a 24hr join period
* Zerg opens at 8pm UTC with no damage delay

#### hSTR Raid:

* Launch at 9pm UTC with a 24hr join period
* Zerg opens at 9pm UTC with no damage delay


### Guild Events

#### Territory Wars

* Follow Officer instructions for setting defenses and which opponent territories to attack

### Guild Rules

#### Discord and Conduct

* Members must have access to Discord & check it daily
* Members must have a SWGOH.gg account
* Be respectful and polite towards others.
* If you will be away from the game for an extended period of time, post in #leave_of_absence

#### Discipline

While warnings and strikes can be handed out for a number of infractions, Beta’s discipline policy is currently under review and will be updated when finalized.

#### Tickets

Achieving 600 Personal Raid Tickets daily is required.

#### Mercing

If you wish to offer yourself as a Raid Merc to another guild, you are allowed to do so but must get permission from an officer. It is very important that you achieve your 600 tickets before leaving the guild. If you will be away from the guild for more than 1 day, coordinate with an officer so that they can find an account to have cover your 600 tickets.

[Join Sigma Alliance!](https://discord.gg/V33Kfaj)
